package com.kus.jwtTest.controller;

import java.io.IOException;
import java.security.Key;
//import org.apache.commons.codec.binary.Base64;
//import com.google.common.io.BaseEncoding;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kus.jwtTest.model.pojo.ClaimPlainText;
import com.kus.jwtTest.model.pojo.DataInquiry;
import com.kus.jwtTest.model.pojo.VACreditRequest;
import com.kus.jwtTest.model.pojo.VACreditReversalRequest;
import com.kus.jwtTest.model.pojo.VARequest;
import com.kus.jwtTest.model.pojo.conts.ResponseConstant;
import com.kus.jwtTest.model.request.BaseRequest;
import com.kus.jwtTest.model.response.BaseResponse;
import com.kus.jwtTest.model.response.CreditVAResponse;
import com.kus.jwtTest.model.response.CreditVARevResponse;
import com.kus.jwtTest.model.response.InquiryVAResponse;
import com.kus.jwtTest.model.response.JwtResponse;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

@RestController
@RequestMapping("/api")
public class MainController {
	
	private final Key key = generateKey();
	private final Key staticKey = generateStaticKey("nu0g/qKTzaGEV3iRspxcP0pgh2rQAEEQocV24iBzD8LvJsWG3Zr6Ll+l1q7AQFVk5+rOwe3xwUHWaF6FUjIuGA==");

	protected final Log LOGGER = LogFactory.getLog(getClass());
	
	/*
	 * Untuk simulasi
	 */
	
	@PostMapping(value="/bank/bank-inq-va")
	public String generateInquiryVA(@RequestBody String req) {
		try {
			Object object = new ObjectMapper().readValue(ResponseConstant.RES_INQ_VA, InquiryVAResponse.class);
			return generateJWTs(object, staticKey);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		
	}
	
	@PostMapping(value="/bank/bank-credit-va")
	public String generateCreditVA(@RequestBody String req) {
		try {
			Object object = new ObjectMapper().readValue(ResponseConstant.RES_CRT_VA, CreditVAResponse.class);
			return generateJWTs(object, staticKey);
		}catch(IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@PostMapping(value="/bank/bank-credit-va-rev")
	public String generateCreditReverseVA(@RequestBody String req) {
		try {
			Object object = new ObjectMapper().readValue(ResponseConstant.RES_CRT_REV_VA, CreditVARevResponse.class);
			return generateJWTs(object, staticKey);
		}catch(IOException e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	/*
	 * untuk debug
	 */
	
	
	@GetMapping(value="/generate")
	public BaseResponse generateToken() {
		BaseResponse res = new BaseResponse();
		res.setResponseCode("OK");
		res.setResponseMessage(generateJWTs(generateDummyVA(),key));
		return res;
	}
	
	@GetMapping(value="/generatestatic")
	public BaseResponse generateStaticToken() {
		BaseResponse res = new BaseResponse();
		res.setResponseCode("OK");
		res.setResponseMessage(generateJWTs(generateDummyVA(), staticKey));
		return res;
		
	}
	
//	@GetMapping(value="/getkey")
//	public BaseResponse getKeys() {
//		BaseResponse res = new BaseResponse();
//		String strKey = Base64.getEncoder().encodeToString(key.getEncoded());
//		LOGGER.info("key generated : "+strKey);
//		res.setResponseCode("OK");
//		res.setResponseMessage(strKey);
//		return res;
//	}
	
	@PostMapping(value="/decode")
	public JwtResponse decodeJwt(@RequestBody BaseRequest req) {
		JwtResponse res = new JwtResponse();
		ClaimPlainText claim = new ClaimPlainText();
		if(req.getStaticToken()) {
			claim = parseJwt(req.getToken(),staticKey);
		}else {
			claim = parseJwt(req.getToken(),key);
		}
		res.setBody(claim.getBody());
		res.setHeader(claim.getHeader());
		return res;
	}
	
	/*
	 * JWT encode and decode
	 */
    
    //encode jwts (self-signed for testing only)
    private String generateJWTs(Object vaRequest, Key key) {
    	//generate JWTs 
    	if(vaRequest instanceof VARequest) {
    		VARequest vaReq = (VARequest) vaRequest;
    		return Jwts.builder()
        			.setHeader(getHeader())
          			.claim("cnl", vaReq.getCnl())
        			.claim("data", vaReq.getData())
        			.setIssuer(vaReq.getIss())
        			.claim("exp_iso8601",vaReq.getExp_iso8601())
        			.claim("iat", vaReq.getIat())
        			.setId(vaReq.getJti())
        			.claim("tid", vaReq.getTid())
        			.signWith(key)
        			.compact();
    	}
    	else if(vaRequest instanceof VACreditRequest) {
    		VACreditRequest vaReq = (VACreditRequest) vaRequest;
    		return Jwts.builder()
        			.setHeader(getHeader())
          			.claim("cnl", vaReq.getCnl())
        			.claim("data", vaReq.getData())
        			.setIssuer(vaReq.getIss())
        			.claim("exp_iso8601",vaReq.getExp_iso8601())
        			.claim("iat", vaReq.getIat())
        			.setId(vaReq.getJti())
        			.claim("tid", vaReq.getTid())
        			.signWith(key)
        			.compact();
    	}
    	else if(vaRequest instanceof VACreditReversalRequest) {
    		VACreditReversalRequest vaReq = (VACreditReversalRequest) vaRequest;
    		return Jwts.builder()
        			.setHeader(getHeader())
          			.claim("cnl", vaReq.getCnl())
        			.claim("data", vaReq.getData())
        			.setIssuer(vaReq.getIss())
        			.claim("exp_iso8601",vaReq.getExp_iso8601())
        			.claim("iat", vaReq.getIat())
        			.setId(vaReq.getJti())
        			.claim("tid", vaReq.getTid())
        			.signWith(key)
        			.compact();
    		}
    	else if(vaRequest instanceof InquiryVAResponse) {
    		InquiryVAResponse vaReq = (InquiryVAResponse) vaRequest;
    		return Jwts.builder()
        			.setHeader(getHeader())
          			.claim("cnl", vaReq.getCnl())
        			.claim("data", vaReq.getData())
        			.setIssuer(vaReq.getIss())
        			.claim("exp_iso8601",vaReq.getExp_iso8601())
        			.claim("iat", vaReq.getIat())
        			.setId(vaReq.getJti())
        			.claim("tid", vaReq.getTid())
        			.signWith(key)
        			.compact();
    	}
    	else if(vaRequest instanceof CreditVAResponse) {
    		CreditVAResponse vaReq = (CreditVAResponse) vaRequest;
    		return Jwts.builder()
        			.setHeader(getHeader())
          			.claim("cnl", vaReq.getCnl())
        			.claim("data", vaReq.getData())
        			.setIssuer(vaReq.getIss())
        			.claim("exp_iso8601",vaReq.getExp_iso8601())
        			.claim("iat", vaReq.getIat())
        			.setId(vaReq.getJti())
        			.claim("tid", vaReq.getTid())
        			.signWith(key)
        			.compact();
    	}
    	else if(vaRequest instanceof CreditVARevResponse) {
    		CreditVARevResponse vaReq = (CreditVARevResponse) vaRequest;
    		return Jwts.builder()
        			.setHeader(getHeader())
          			.claim("cnl", vaReq.getCnl())
        			.claim("data", vaReq.getData())
        			.setIssuer(vaReq.getIss())
        			.claim("exp_iso8601",vaReq.getExp_iso8601())
        			.claim("iat", vaReq.getIat())
        			.setId(vaReq.getJti())
        			.claim("tid", vaReq.getTid())
        			.signWith(key)
        			.compact();
    	}
    	else {
    			return "ERROR class not match";
    		}
    	
    	
    }
    
    //generate keys (auto generated for now)
    private Key generateKey() {
    	return Keys.secretKeyFor(SignatureAlgorithm.HS512);
    }
    
    //generate static key
    private Key generateStaticKey(String encodedKey) {
    	byte[] decodedKey = DatatypeConverter.parseBase64Binary(encodedKey);
    	//byte[] decodedKey = BaseEncoding.base64().decode(encodedKey);
    	return new SecretKeySpec(decodedKey,0,decodedKey.length,"HmacSHA512");
    }
    
    
    //get header (hardcoded for now)
    private Map<String,Object> getHeader(){
    	Map<String,Object> header = new HashMap<String,Object>();
    	header.put("alg", "HS512");
    	header.put("typ", "JWT");
    	return header;
    }
    
    //generate dummy VA
    private VARequest generateDummyVA() {
    	VARequest dummy = new VARequest();
    	DataInquiry data = new DataInquiry();
    	data.setVaNo("12345678");
    	dummy.setData(data);
    	dummy.setIss("--");
    	dummy.setExp_iso8601("2019-03-05T08:45:51+07:00");
    	dummy.setJti("63726483265763");
    	dummy.setIat("1516239022");
    	dummy.setTid("001");
    	return dummy;
    }
    
    //for decoding JWTs message
    private ClaimPlainText parseJwt(String req, Key key){
    	Jws<Claims> claim = null;
    	ClaimPlainText result = new ClaimPlainText();
    	try {
    		claim = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(req);
        	try {
        		result.setHeader(new ObjectMapper().writeValueAsString(claim.getHeader()));
        		result.setBody(new ObjectMapper().writeValueAsString(claim.getBody()));
        	}catch(Exception e) {
        		result.setHeader("ERROR");
        		result.setBody(e.getMessage());
        	}
    	}catch (JwtException e) {
    		e.printStackTrace();
    		result.setHeader("ERROR");
    		result.setBody(e.getMessage());
    	}
    	return result;
    	
    }
    
    
}
