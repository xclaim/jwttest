package com.kus.jwtTest.model.pojo;

public class DataCreditReversal {
	
	private String trxAmount;
	private String reffNoRev;
	private String trfFrom;
	private String vaNo;
	
	public String getTrxAmount() {
		return trxAmount;
	}
	public void setTrxAmount(String trxAmount) {
		this.trxAmount = trxAmount;
	}
	public String getReffNoRev() {
		return reffNoRev;
	}
	public void setReffNoRev(String reffNoRev) {
		this.reffNoRev = reffNoRev;
	}
	public String getTrfFrom() {
		return trfFrom;
	}
	public void setTrfFrom(String trfFrom) {
		this.trfFrom = trfFrom;
	}
	public String getVaNo() {
		return vaNo;
	}
	public void setVaNo(String vaNo) {
		this.vaNo = vaNo;
	}
	
	

}
