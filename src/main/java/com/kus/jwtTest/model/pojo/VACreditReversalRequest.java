package com.kus.jwtTest.model.pojo;

public class VACreditReversalRequest extends BaseJwt{

	private DataCreditReversal data;

	public DataCreditReversal getData() {
		return data;
	}

	public void setData(DataCreditReversal data) {
		this.data = data;
	}
	
}
