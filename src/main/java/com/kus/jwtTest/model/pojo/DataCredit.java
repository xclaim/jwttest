package com.kus.jwtTest.model.pojo;

public class DataCredit {

	private String trxAmount;
	private String narative;
	private String trfFrom;
	private String narativeExt;
	private String vaNo;

	public String getTrxAmount() {
		return trxAmount;
	}

	public void setTrxAmount(String trxAmount) {
		this.trxAmount = trxAmount;
	}

	public String getNarative() {
		return narative;
	}

	public void setNarative(String narative) {
		this.narative = narative;
	}

	public String getTrfFrom() {
		return trfFrom;
	}

	public void setTrfFrom(String trfFrom) {
		this.trfFrom = trfFrom;
	}

	public String getNarativeExt() {
		return narativeExt;
	}

	public void setNarativeExt(String narativeExt) {
		this.narativeExt = narativeExt;
	}

	public String getVaNo() {
		return vaNo;
	}

	public void setVaNo(String vaNo) {
		this.vaNo = vaNo;
	}
	   
	   
}
