package com.kus.jwtTest.model.pojo.conts;

public final class ResponseConstant {
	

	public static final String RES_INQ_VA = "{\"cnl\":\"EDC\",\"data\":{\"institutionId\":8,\"accPoolingName\":\"Kementerian Sosial Republik Indonesia\",\"currentBalance\":\"590000.00\",\"trxMin\":\"\",\"restOfBill\":\"0.00\",\"custName\":\"give me card\",\"type\":\"OPNB\",\"isActive\":1,\"cardNo\":\"112\",\"accPooling\":\"11223344\",\"lastTrxDt\":\"2019-03-07T15:11:20.000+07:00\",\"descr\":\"-\",\"createdAt\":\"2019-03-03T03:01:33.000+07:00\",\"feeAmount\":1000,\"billAmount\":\"\",\"custEmail\":null,\"trxMax\":\"\",\"id\":1,\"vaNo\":\"6677812121212121\",\"invoiceNo\":\"6677812121212121\",\"expiredDt\":\"2019-04-01T03:01:00.000+07:00\",\"custPhoneNo\":null,\"status\":\"AVAILABLE\",\"updatedAt\":\"2020-02-21T21:32:51.000+07:00\"},\"iss\":\"--\",\"exp_iso8601\":\"2020-03-17T14:16:06.559+07:00\",\"iat\":1584429066559,\"jti\":\"63726483265763\",\"tid\":\"001\"}";
	public static final String RES_CRT_VA = "{\"cnl\":\"EDC\",\"data\":{\"institutionId\":6,\"accPoolingName\":\"Yayasan Unissula\",\"currentBalance\":\"17000.00\",\"trxMin\":\"\",\"restOfBill\":\"183000.00\",\"custName\":\"Cust Baru C\",\"type\":\"OPNP\",\"isActive\":1,\"cardNo\":\"-\",\"accPooling\":\"2313112414\",\"lastTrxDt\":\"2020-02-24T09:40:18.000+07:00\",\"descr\":\"Descr 7\",\"createdAt\":\"2020-02-23T13:38:08.000+07:00\",\"feeAmount\":3000,\"billAmount\":\"200000.00\",\"custEmail\":\"Email 7\",\"trxMax\":\"\",\"id\":26,\"vaNo\":\"5678951758393665\",\"invoiceNo\":\"1000007b\",\"expiredDt\":\"2021-12-06T22:01:06.000+07:00\",\"custPhoneNo\":\"Phone 7\",\"status\":\"AVAILABLE\",\"updatedAt\":\"2020-02-24T09:40:18.000+07:00\"},\"iss\":\"--\",\"exp_iso8601\":\"2020-03-17T14:36:43.057+07:00\",\"iat\":1584430303057,\"jti\":\"997221012\",\"tid\":\"001\"}";
	public static final String RES_CRT_REV_VA = "{\"cnl\":\"EDC\",\"data\":{\"trxAmount\":17000,\"reffNoRev\":\"997221012\",\"trfFrom\":\"123456\",\"vaNo\":\"5678951758393665\",\"accPooling\":\"2313112414\",\"accPoolingName\":\"Yayasan Unissula\",\"feeAmount\":3000},\"iss\":\"--\",\"exp_iso8601\":\"2020-03-17T14:39:22.986+07:00\",\"iat\":1584430462986,\"jti\":\"592122410\",\"tid\":\"001\"}"; 
	
	
	
	
	
}
