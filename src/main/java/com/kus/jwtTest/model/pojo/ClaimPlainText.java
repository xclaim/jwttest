package com.kus.jwtTest.model.pojo;

public class ClaimPlainText {
	private String header;
	private String body;
	
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	

}
