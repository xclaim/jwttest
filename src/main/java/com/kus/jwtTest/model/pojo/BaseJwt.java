package com.kus.jwtTest.model.pojo;

public class BaseJwt {
	
		private String cnl;
	    private String iss;
	    private String exp_iso8601;
	    private String iat;
	    private String jti;
	    private String tid;
	    
		public String getCnl() {
			return cnl;
		}
		public void setCnl(String cnl) {
			this.cnl = cnl;
		}
		public String getIss() {
			return iss;
		}
		public void setIss(String iss) {
			this.iss = iss;
		}
		public String getExp_iso8601() {
			return exp_iso8601;
		}
		public void setExp_iso8601(String exp_iso8601) {
			this.exp_iso8601 = exp_iso8601;
		}
		public String getIat() {
			return iat;
		}
		public void setIat(String iat) {
			this.iat = iat;
		}
		public String getJti() {
			return jti;
		}
		public void setJti(String jti) {
			this.jti = jti;
		}
		public String getTid() {
			return tid;
		}
		public void setTid(String tid) {
			this.tid = tid;
		}
	    
	    

}
