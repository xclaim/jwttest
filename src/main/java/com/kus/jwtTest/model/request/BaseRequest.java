package com.kus.jwtTest.model.request;

public class BaseRequest {
	private String token;
	private boolean staticToken;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean getStaticToken() {
		return staticToken;
	}

	public void setStaticToken(boolean staticToken) {
		this.staticToken = staticToken;
	}
	
	
	
	

}
