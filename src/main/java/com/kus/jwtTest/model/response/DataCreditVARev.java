package com.kus.jwtTest.model.response;

import java.math.BigDecimal;

public class DataCreditVARev {
	
	private BigDecimal trxAmount;
	private String reffNoRev;
	private String trfFrom;
	private String vaNo;
	private String accPooling;
	private String accPoolingName;
	private BigDecimal feeAmount;
	
	public BigDecimal getTrxAmount() {
		return trxAmount;
	}
	public void setTrxAmount(BigDecimal trxAmount) {
		this.trxAmount = trxAmount;
	}
	public String getReffNoRev() {
		return reffNoRev;
	}
	public void setReffNoRev(String reffNoRev) {
		this.reffNoRev = reffNoRev;
	}
	public String getTrfFrom() {
		return trfFrom;
	}
	public void setTrfFrom(String trfFrom) {
		this.trfFrom = trfFrom;
	}
	public String getVaNo() {
		return vaNo;
	}
	public void setVaNo(String vaNo) {
		this.vaNo = vaNo;
	}
	public String getAccPooling() {
		return accPooling;
	}
	public void setAccPooling(String accPooling) {
		this.accPooling = accPooling;
	}
	public String getAccPoolingName() {
		return accPoolingName;
	}
	public void setAccPoolingName(String accPoolingName) {
		this.accPoolingName = accPoolingName;
	}
	public BigDecimal getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}
	
	
	

}
