package com.kus.jwtTest.model.response;

public class InquiryVAResponse extends BaseJwtResponse{

	private DataInquiryVA data;

	public DataInquiryVA getData() {
		return data;
	}

	public void setData(DataInquiryVA data) {
		this.data = data;
	}
	
	
}
