package com.kus.jwtTest.model.response;

public class Body {
	
	private String iss;
	private String expIso8601;
	private String jti;
	private String iat;
	private Data data;
	
	public String getIss() {
		return iss;
	}
	public void setIss(String iss) {
		this.iss = iss;
	}
	public String getExpIso8601() {
		return expIso8601;
	}
	public void setExpIso8601(String expIso8601) {
		this.expIso8601 = expIso8601;
	}
	public String getJti() {
		return jti;
	}
	public void setJti(String jti) {
		this.jti = jti;
	}
	public String getIat() {
		return iat;
	}
	public void setIat(String iat) {
		this.iat = iat;
	}
	public Data getData() {
		return data;
	}
	public void setData(Data data) {
		this.data = data;
	}
	
	

	
	
	

}
