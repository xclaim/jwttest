package com.kus.jwtTest.model.response;

public class CreditVARevResponse extends BaseJwtResponse{

	private DataCreditVARev data ;

	public DataCreditVARev getData() {
		return data;
	}

	public void setData(DataCreditVARev data) {
		this.data = data;
	}
	
	
}
