package com.kus.jwtTest.model.response;

public class CreditVAResponse extends BaseJwtResponse{

	private DataCreditVA data;

	public DataCreditVA getData() {
		return data;
	}

	public void setData(DataCreditVA data) {
		this.data = data;
	}
	
}
