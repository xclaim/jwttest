package com.kus.jwtTest.model.response;
import java.math.BigDecimal;

public class DataCreditVA {

	private Long institutionId;
	private String accPoolingName;
	private String currentBalance;
	private String trxMin;
	private String restOfBill;
	private String custName;
	private String type;
	private Integer isActive;
	private String cardNo;
	private String accPooling;
	private String lastTrxDt;
	private String descr;
	private String createdAt;
	private BigDecimal feeAmount;
	private String billAmount;
	private String custEmail;
	private String trxMax;
	private Long id;
	private String vaNo;
	private String invoiceNo;
	private String expiredDt;
	private String custPhoneNo;
	private String status;
	private String updatedAt;
	
	public Long getInstitutionId() {
		return institutionId;
	}
	public void setInstitutionId(Long institutionId) {
		this.institutionId = institutionId;
	}
	public String getAccPoolingName() {
		return accPoolingName;
	}
	public void setAccPoolingName(String accPoolingName) {
		this.accPoolingName = accPoolingName;
	}
	public String getCurrentBalance() {
		return currentBalance;
	}
	public void setCurrentBalance(String currentBalance) {
		this.currentBalance = currentBalance;
	}
	public String getTrxMin() {
		return trxMin;
	}
	public void setTrxMin(String trxMin) {
		this.trxMin = trxMin;
	}
	public String getRestOfBill() {
		return restOfBill;
	}
	public void setRestOfBill(String restOfBill) {
		this.restOfBill = restOfBill;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public String getCardNo() {
		return cardNo;
	}
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	public String getAccPooling() {
		return accPooling;
	}
	public void setAccPooling(String accPooling) {
		this.accPooling = accPooling;
	}
	public String getLastTrxDt() {
		return lastTrxDt;
	}
	public void setLastTrxDt(String lastTrxDt) {
		this.lastTrxDt = lastTrxDt;
	}
	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public BigDecimal getFeeAmount() {
		return feeAmount;
	}
	public void setFeeAmount(BigDecimal feeAmount) {
		this.feeAmount = feeAmount;
	}
	public String getBillAmount() {
		return billAmount;
	}
	public void setBillAmount(String billAmount) {
		this.billAmount = billAmount;
	}
	public String getCustEmail() {
		return custEmail;
	}
	public void setCustEmail(String custEmail) {
		this.custEmail = custEmail;
	}
	public String getTrxMax() {
		return trxMax;
	}
	public void setTrxMax(String trxMax) {
		this.trxMax = trxMax;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVaNo() {
		return vaNo;
	}
	public void setVaNo(String vaNo) {
		this.vaNo = vaNo;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getExpiredDt() {
		return expiredDt;
	}
	public void setExpiredDt(String expiredDt) {
		this.expiredDt = expiredDt;
	}
	public String getCustPhoneNo() {
		return custPhoneNo;
	}
	public void setCustPhoneNo(String custPhoneNo) {
		this.custPhoneNo = custPhoneNo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
}
